module.exports.autoreload = {
  active: true,
  usePolling: false,
  dirs: [
    "api/models",
    "api/controllers",
    "api/policies",
    "api/services",
    "config/",
    "views/"
  ],
  ignored: [
    // Ignore all files with .ts extension 
    "**.ts"
  ]
};