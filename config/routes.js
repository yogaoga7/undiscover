/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

 module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  },

  // OAuth endpoinds
  'post /auth/:provider/login': 'AuthController.loginProvider',
  'post /auth/:provider/register': 'AuthController.registerProvider',
  'post /auth/login': 'AuthController.login',
  'post /auth/register': 'AuthController.register',
  'post /auth/password/forgot' : 'AuthController.forgotPassword',
  'get /auth/password/reset/configirm' : 'AuthController.resetPassword',
  'post /auth/password/verify' : 'AuthController.verifyPassword',

  // User Endpoints
  'get /users/feed': 'UserController.timeline',
  'get /users/:user_id': 'UserController.profile',
  'get /users/self/feed': 'UserController.feed',

  // Video Endpoints
  'post /videos/update': 'VideoController.create',
  'put /videos/:video_id/update': 'VideoController.update',
  'delete /videos/:video_id/delete': 'VideoController.destroy',

  // Like Endpoints
  'post /videos/:video_id/like' : 'VideoActivity.like',
  'delete /videos/:video_id/like' : 'VideoActivity.unlike',
  'get /videos/:video_id/like': 'VideoActivity.list',

  // Comment Endpoints
  'post /videos/:video_id/comments': 'CommentController.store',
  'get /videos/:video_id/comments': 'CommentController.index',
  'delete /videos/:video_id/comments/:comment_id': 'CommentController.destroy',
  
  'post /genre': 'GenreController.create',
  'get /genres': 'GenreController.findAll',


  // Relationship Endpoints
  'post /users/:user_id/relationship': 'UserController.relationship',
  'get /users/:user_id/fans': 'UserController.fans',
  'get /users/:user_id/following': 'UserController.following',

  // routes playlist
  'get /playlists': 'PlaylistController.index',
  'get /playlists/:playlist_id': 'PlaylistController.show',
  'post /playlists': 'PlaylistController.store',
  'put /playlists/:playlist_id': 'PlaylistController.update',
  'post /playlists/:playlist_id/track': 'PlaylistController.storeTrack',
  'delete /playlists/:playlist_id': 'PlaylistController.destroy',
  'delete /playlists/:playlist_id/track/:video_id': 'PlaylistController.destroyTrack',

  //Route Search 
  'get /video/search': 'SearchController.search',
  'get /tags/search' : 'SearchController.findHastag',

  // API Documentations endpoint
  '/docs/endpoints/': {
    view: 'api/index',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/authentication': {
    view: 'api/authentication',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/users': {
    view: 'api/users',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/comments': {
    view: 'api/comments',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/playlists': {
    view: 'api/playlists',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/authentication/social/register': {
    view: 'api/register_social',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/authentication/login': {
    view: 'api/login',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/authentication/social/login': {
    view: 'api/login_social',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/authentication/password/forgot': {
    view: 'api/password_forgot',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/comments/create': {
    view: 'api/comments/create',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/comments/delete': {
    view: 'api/comments/delete',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/comments/get': {
    view: 'api/comments/get',
    locals: {
      layout: 'api/docs'
    }
  },
  
  '/docs/endpoints/likes': {
    view: 'api/like',
    locals: {
      layout: 'api/docs'
    }
  }, 

  '/docs/endpoints/genres': {
    view: 'api/genre',
    locals: {
      layout: 'api/docs'
    }
  },

  '/docs/endpoints/videos': {
    view: 'api/video',
    locals: {
      layout: 'api/docs'
    }
  }, 

  '/docs/endpoints/videos': {
    view: 'api/videos',
    locals: {
      layout: 'api/docs'
    }
  },  

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
