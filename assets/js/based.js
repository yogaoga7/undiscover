$(document).ready(function(){
  if($(window).width() < 768){
    $(".menu-docs-top").on("click", function(){
      if($(".menu-docs").hasClass("close-menu")){
        $(".menu-docs").slideUp();
        $(".menu-docs").removeClass("close-menu");
        $(".menu-docs-icon").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
      }else{
        $(".menu-docs").slideDown();
        $(".menu-docs").addClass("close-menu");
        $(".menu-docs-icon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
      }
    })
  }else{
    $(".menu-docs").attr("style", "display:block;");

    $('.menu-docs').affix({
    offset: {
      top: 200,
      bottom: function () {
        return (this.bottom = $('.footer').outerHeight(true))
      }
    }
  })
  }
});