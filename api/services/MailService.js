var nodemailer = require('nodemailer');
var fs = require('fs-extra-promise');
var ejs = require('ejs');
var qs = require('querystring');
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'noreply.valdlabs@gmail.com',
    pass: 'vldlbsRSCH'
  }
});

module.exports = {
  send: function (templateName, data) {
    return fs
    .readFileAsync('views/mail/' + templateName + '.ejs')
    .then(function (template) {
      var templateContent = qs.stringify({
        template: templateName,
        content: JSON.stringify(data.content)
      });
      var mailOptions = {
        from    : data.from,
        to      : data.to,
        subject : data.subject,
        // html    : {path: sails.getBaseUrl() + '/mail/view?' + templateContent}
        html    : "<div style='font-size:22px;line-height:32px;width:480px;margin:0 auto;color:#666;'><h4 style='text-align:center;'>Undiscovered</h4><hr style='margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;'/><h3>Hey " + data.content.first_name + " " + data.content.last_name + ",</h3><p>We received a request to change your password on Undiscovered.</p><p style='padding:20px 0; text-align:center;'><a style='color:#2b5a83;text-decoration:none;border-radius:3px;border:1px solid #2b5a83;padding:10px 19px 12px 19px;font-size:17px;font-weight:500;white-space:nowrap;border-collapse:collapse;display:inline-block;line-height:19px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif' href='http://undiscovered.herokuapp.com/auth/password/reset/confirm?token=" + data.content.token + "'>Reset password</a><p>If you ignore this message, your password won't be changed.</p><br/><br/><hr style='margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;'/><p style='font-size:14px;line-height:16px;color:#777;'>Your friends at Undiscovered.</p><p style='font-size:12px;line-height:14px;color:#ddd;'>by valdlabs research.</p></div>"
      };

      return new Promise(function (resolve, reject) {
        transporter.sendMail(mailOptions, function (err, info) {
          if(err) return reject(err);

          resolve(info);
        });
      });
    });
  }
};