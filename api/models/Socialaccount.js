/**
* Socialaccount.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'socialaccounts',
	attributes: {
		uid: {
			type: 'text',
			unique: true,
			required: true
		},
		username: {
			type: 'text',
			unique: true,
			required: true
		},
		provider: {
			type: 'string',
			required: true
		},
		name: {
			type: 'string'
		},
		avatar: {
			type: 'string',
			required: true
		},
		access_token: { 
			type: 'text', 
			required: true
		},
		is_deleted: { 
			type: 'boolean', 
			defaultsTo : false 
		},
		user: {
			model:'user'
		},
		toJSON: function () {
      var obj = this.toObject();
      delete obj.is_deleted;
      return obj;
    },
	},
	compareAccessToken : function (access_token, social_account, cb) {
		if(access_token == social_account.access_token) {
			cb(null, true);
		} else {
			cb(null, false);
		}
	}
};