/**
* Hastag.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'hastags',
  attributes: {
  	user : {
  		model : 'user',
  		required: true
  	},
  	hastag : {
  		type: 'string'
  	},
  	videos : {
  		collection: 'video',
      via: 'hastags'
  	},
  	is_deleted: {
			type: 'boolean', 
			defaultsTo : false 
		},
		
  }
};

