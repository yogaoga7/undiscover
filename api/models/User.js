/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcryptjs');
var suid = require('rand-token').suid;

module.exports = {

	tableName: 'users',
	attributes: {
		email : { 
			type: 'email', 
			unique: true
		},
		first_name: { 
			type: 'string', 
			required: true 
		},
		last_name: { 
			type: 'string'
		},
		avatar: { 
			type: 'string'
		},
		password: {
			type: 'string',
			required: true
		},
		location_lat: { 
			type: 'string' 
		},
		location_long: { 
			type: 'string'
		},
		biography: { 
			type: 'text'
		},
		token: { 
			type: 'text', 
			// required: true,
		},
		is_deleted: { 
			type: 'boolean', 
			defaultsTo : false 
		},
		socialaccounts: {
			collection: 'socialaccount',
			via: 'user'
		},
		videos: {
			collection: 'video',
			via: 'user'
		},

		toJSON: function () {
			var obj = this.toObject();
			obj.avatar = obj.avatar || null;
			obj.biography = obj.biography || null;
			obj.location_lat = obj.location_lat || null;
			obj.location_long = obj.location_long || null;
			delete obj.token;
			delete obj.password;
			delete obj.is_deleted;
			return obj;
		},
		changePassword: function(newPassword, cb){
      this.newPassword = newPassword;
      this.save(function(err, u) {
        return cb(err,u);
      });
    },
	},
	// Here we encrypt password before creating a User
	beforeCreate : function (values, next) {
		bcrypt.genSalt(10, function (err, salt) {
			if(err) return next(err);
			bcrypt.hash(values.password, salt, function (err, hash) {
				if(err) return next(err);
				values.password = hash;
				values.token = suid(30);
				next();
			})
		});
	},

	beforeUpdate: function(values, next) {
		function hashPassword(values, next) {
			bcrypt.hash(values.password, 10, function(err, hash) {
				if (err) {
					return next(err);
				}
				values.password = hash;
				next();
			});
		}
		if (values.password) {
			hashPassword(values, next);
		}
		else {
			User.findOne(values.id).done(function(err, user) {
				if (err) {
					next(err);
				}
				else {
					values.password = user.password;
					next();
				}
			});
		}
	},

	comparePassword : function (password, user, cb) {
		bcrypt.compare(password, user.password, function (err, match) {

			if(err) cb(err);
			if(match) {
				cb(null, true);
			} else {
				cb(err);
			}
		});
	}


};

