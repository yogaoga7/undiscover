/**
* VideoActivity.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'video_activities',
	attributes: {
		user: {
			model: "user"	
		},
		video: {
			model: "video"
		},
		type: {
			type: "string",
			enum: ['like','unlike','view']
		},
		is_deleted: {
			type: 'boolean', 
			defaultsTo : false 
		},
		video: {
			model: 'video'
		},
		toJSON: function () {
      var obj = this.toObject();
      delete obj.is_deleted;
      return obj;
    },
	}
};

