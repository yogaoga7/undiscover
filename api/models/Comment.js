/**
* Comment.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'comments',
  attributes: {
    content: {
      type: 'string', 
      required: true
    },
    is_deleted: {
      type: 'boolean',
      defaultsTo: false
    },
    user: {
      model: 'user'
    },
    video: {
      model: 'video'
    },
    timestamp: {
      type: 'float',
      defaultsTo: function () {
        return new Date().getTime();
      }
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.is_deleted;
      return obj;
    },
  }
};

