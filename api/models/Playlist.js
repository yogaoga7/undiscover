/**
* Playlist.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'playlists',
  attributes: {
    title: {
      type: 'string',
      required: true
    },
    description: {
      type: 'text'
    },
    user: {
      model: 'user',
      required: true
    },
    videos: {
      collection: 'video',
      via: 'playlists'
    }
  }
};

