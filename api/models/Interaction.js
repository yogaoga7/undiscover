/**
* Interaction.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'interactions',
  attributes: {
    sender_id: {
      type: 'string',
      required: true
    },
    receiver_id: {
      type: 'string',
      required: true
    },
    status: {
      type: 'string',
      enum: ['block', 'follow'],
      required: true
    },
    is_deleted: {
      type: 'boolean', 
      defaultsTo : false
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.is_deleted;
      return obj;
    },
  }
};

