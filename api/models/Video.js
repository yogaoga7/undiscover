/**
* Videos.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'videos',
	attributes: {
		// id : {
		// 	type: 'integer',
		// 	primaryKey: true,
		// 	autoIncrement: true
		// },
		song_title: {
			type: 'string',
			required: true 
		},
		artist: {
			type: 'string',
			required: true 
		},
		thumblains: {
			type: 'string',
		},
		video: {
			type: 'string',
		},
		location: {
			type: 'string',
		},
		user: {
			model:'user',
		},
		activities: {
			collection: 'videoactivity',
			via: 'video'
		},
		comments: {
			collection: 'comment',
			via: 'video'
		},
		genres: {
			collection: 'genre',
			via: 'videos'
		},
		playlists: {
			collection: 'playlist',
			via: 'videos'
		},
		hastags: {
			collection: 'hastag',
			via: 'videos'
		}, 
		is_deleted: {
			type: 'boolean', 
			defaultsTo : false 
		},
		caption: {
			type: 'string',
		},
		timestamp: {
			type: 'float',
			defaultsTo: function () {
				return new Date().getTime();
			}
		},
		toJSON: function () {
      var obj = this.toObject();
      if(obj.comments){
      	var comments = obj.comments;
      	delete obj.comments;
      	obj.comments = {data: comments.slice(0,3), count: comments.length};
      }
      if(obj.activities){
      	var likes = _.where(obj.activities, {type: 'like'});
	      var views = _.where(obj.activities, {type: 'view'});
	      delete obj.activities;
	      delete obj.is_deleted;
	      var likers = likes.slice(0,3) || [];
	      var viewers = views.slice(0,3) || [];
	      obj.likes = {data: likers, count: likers.length};
      	obj.views = {data: viewers, count: viewers.length};
      }
      return obj;
    },
	}
};

