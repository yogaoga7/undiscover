/**
 * canAccess
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */

module.exports = function (req, res, next) {
  // User is allowed, proceed to the next policy, 
  // or if this is the last policy, the controller
  var token = req.headers.authorization; 
  if(token){
    User.findOne({token:token})
    .exec(function (err, user) {
      if(err) return next(err);
      
      if(!user) return res.json(400, {status: 'FAILED', message: 'InvalidToken'});

      delete user.token;
      req.user = user;
      return next();
    });
  } else {
    // User is not allowed
    return res.json(400, {status: 'FAILED', message: 'Unauthorized'});
  }

}