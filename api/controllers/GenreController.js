/**
 * GenreController
 *
 * @description :: Server-side logic for managing Genres
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	create: function (req, res){
 		var name = req.param('name');

 		Genre.create({name: name}).exec(function (err, genre){
 			if(err){
 				return res.json(400, {status: "FAILED"});
 			}
 			if(!genre){
 				return res.json(400, {status: "ERROR"});
 			}
 			return res.json(200, {status: "OK", result: {genre: genre}});
 		});
 	},

 	findAll: function (req,res){
 		Genre.find(function (err, genre) {
 			if (err) {
 				res.send(400);
 			} else {
 				res.send(genre);
 			}
 		});
 	},

 };

