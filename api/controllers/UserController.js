/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

  profile: function (req, res) {
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var userId = req.param('user_id');

    // make sure all the parameters sent
    if(!userId){
      missingParams.push('user_id');
    }

    if(missingParams.length > 0){
      isError = true;
      message = "MissingParameter";
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
    }
    if(!isError){
      User.findOne({id: userId})
      .exec(function (errorUser, user) {
        if(errorUser){
          var errorString = JSON.stringify(errorUser);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[errorUser.code] ? HTTP_STATUS[errorUser.code] : 400;
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        if(!user){
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'NotFound'});
        }

        var Q = require('q');
        Q.all([
          Video.count({
            user: userId
          }).then(),
          Interaction.count({
            is_deleted: false,
            sender_id: userId,
            status: 'follow'
          }).then(),
          Interaction.count({
            is_deleted: false,
            receiver_id: userId,
            status: 'follow'
          }).then()
          ])
        .spread(function (videoCount, fansCount, followingCount){
          user.counts = {
            songs: videoCount,
            fans: fansCount,
            following: followingCount
          };
          return res.json(HTTP_STATUS.OK, {status: 'OK', data: user});
        })
        .fail(function (err){
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: "FAILED", err: err});
        }); 
      })
    } else {
      res.json(HTTP_STATUS.BAD_REQUEST, {status: "FAILED", message: message});
    }
  },

  feed: function (req, res) {
    // get parameter
    var token = req.headers.authorization;
    var since = req.param('since');
    var until = req.param('until');
    var limit = req.param('limit');

    // if limit not sent, set standard value to 10
    if(!limit)
      limit = 10;

    var query = Video.find({user: req.user.id})
    .sort('createdAt DESC');
    if(since)
      query.where({timestamp: {'>': since}});
    else if(until)
      query.where({timestamp: {'<': until}});
    query
    .limit(limit)
    .populate('activities', {is_deleted: false, sort: 'createdAt DESC'})
    .populate('comments', {is_deleted: false, sort: 'createdAt DESC'})
    .populate('genres')
    .populate('user')
    .exec(function (e, videos) {
      if(e){
        var errorString = JSON.stringify(e);
        var errorObject = JSON.parse(errorString);
        var code = HTTP_STATUS[e.code] ? HTTP_STATUS[e.code] : 400;
        return res.json(code, {status: 'FAILED', message: errorObject.summary});
      }

      return res.json(HTTP_STATUS.OK, {status: "OK", data: videos});
    });
  },

  timeline: function (req, res) {

    // get parameter
    var token = req.headers.authorization;
    var since = req.param('since');
    var until = req.param('until');
    var limit = req.param('limit');

    // if limit not sent, set standard value to 10
    if(!limit)
      limit = 10;

    // get user by token
    User.findOne({token: token})
    .exec(function (err, user) {
      if(err){
        var errorString = JSON.stringify(err);
        var errorObject = JSON.parse(errorString);
        var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
        return res.json(code, {status: 'FAILED', message: errorObject.summary});
      }

      // get following of user
      Interaction.find({sender_id: user.id, status: 'follow', is_deleted: false})
      .exec(function (er, following) {
        if(er){
          var errorString = JSON.stringify(er);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[er.code] ? HTTP_STATUS[er.code] : 400;
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        var userIds = [];

        if(following){
          following.forEach(function (value, key) {
            userIds.push(value.receiver_id);
          });
        }
        userIds.push(user.id);

        var query = Video.find({user: userIds})
        .sort('createdAt DESC');
        if(since)
          query.where({timestamp: {'>': since}});
        else if(until)
          query.where({timestamp: {'<': until}});
        query
        .limit(limit)
        .populate('activities', {is_deleted: false, sort: 'createdAt DESC'})
        .populate('comments', {is_deleted: false, sort: 'createdAt DESC'})
        .populate('genres')
        .populate('user')
        .exec(function (e, videos) {
          if(e){
            var errorString = JSON.stringify(e);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[e.code] ? HTTP_STATUS[e.code] : 400;
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }

          return res.json(HTTP_STATUS.OK, {status: "OK", data: videos});
        });
      });
    });
  },

  relationship: function (req, res) {

    // get parameter
    var userId = req.param('user_id');
    var action = req.param('action');

    var isError = false;
    var message = null;
    var missingParams = [];

    // make sure all the parameters sent
    if(!userId){
      missingParams.push('user_id');
    }
    if(!action){
      missingParams.push('action');
    }

    if(missingParams.length > 0){
      isError = true;
      message = "MissingParameter";
    }

    if(!isError){

      // get user by token
      var token = req.headers.authorization;
      User.findOne({token:token})
      .exec(function (error, user) {
        if (error) {
          var errorString = JSON.stringify(error);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[error.code] ? HTTP_STATUS[error.code] : 400;
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        var senderId = user.id;
        var status;
        if(action == 'unfollow' || action == 'follow')
          status = 'follow';
        else if(action == 'block' || action == 'unblock')
          status = 'block';
        var fields = {
          sender_id: senderId, 
          receiver_id: userId, 
          status: status
        };
        Interaction.findOrCreate(fields)
        .exec(function (err, interaction) {
          if(err){
            var errorString = JSON.stringify(err);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }

          if(action == 'follow' || action == 'block'){
            interaction.is_deleted = false;
            interaction.save();
          } else if(action == 'unfollow' || action == 'unblock'){
            interaction.is_deleted = true;
            interaction.save();
          }

          return res.json(HTTP_STATUS.OK, {status: "OK", message: action+'ed'});
        });
      });
    } else {
      res.json(HTTP_STATUS.BAD_REQUEST, {status: "FAILED", message: message});
    }
  },

  fans: function (req, res) {

    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var userId = req.param('user_id');

    // make sure all the parameters sent
    if(!userId){
      missingParams.push('user_id');
    }

    if(missingParams.length > 0){
      isError = true;
      message = "MissingParameter";
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
    }

    if(!isError){
      Interaction
      .find({receiver_id: userId, status: 'follow', is_deleted: false})
      .exec(function (errorFollowers, followers) {
        if (errorFollowers) {
          var errorString = JSON.stringify(errorFollowers);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[errorFollowers.code] ? HTTP_STATUS[errorFollowers.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        var userIds = _.pluck(followers, 'sender_id');
        User.find({id: userIds})
        .sort('first_name ASC')
        .exec(function (errorUser, users) {
          if(errorUser){
            var errorString = JSON.stringify(errorUser);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[errorUser.code] ? HTTP_STATUS[errorUser.code] : 400;
            sails.log.error(req.method + " "+ req.path + ": " + errorString);
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }

          if(users){
            var users = _.map(users, function (value, index) {
              delete value.token;
              return value;
            });
          }

          return res.json(HTTP_STATUS.OK, {status: 'OK', data: users});
        });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, {status: "FAILED", message: message});
    }
  },

  following: function (req, res) {
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var userId = req.param('user_id');

    // make sure all the parameters sent
    if(!userId){
      missingParams.push('user_id');
    }

    if(missingParams.length > 0){
      isError = true;
      message = "MissingParameter";
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
    }

    if(!isError){
      Interaction
      .find({sender_id: userId, status: 'follow', is_deleted: false})
      .exec(function (errorFollowers, followers) {
        if (errorFollowers) {
          var errorString = JSON.stringify(errorFollowers);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[errorFollowers.code] ? HTTP_STATUS[errorFollowers.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        var userIds = _.pluck(followers, 'receiver_id');
        User.find({id: userIds})
        .sort('first_name ASC')
        .exec(function (errorUser, users) {
          if(errorUser){
            var errorString = JSON.stringify(errorUser);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[errorUser.code] ? HTTP_STATUS[errorUser.code] : 400;
            sails.log.error(req.method + " "+ req.path + ": " + errorString);
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }

          if(users){
            var users = _.map(users, function (value, index) {
              delete value.token;
              return value;
            });
          }

          return res.json(HTTP_STATUS.OK, {status: 'OK', data: users});
        });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, {status: "FAILED", message: message});
    } 
  }

};

