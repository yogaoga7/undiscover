/**
 * CommentController
 *
 * @description :: Server-side logic for managing comments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  index: function (req, res) {
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var videoId = req.param('video_id');
    var since = req.param('since');
    var until = req.param('until');
    var limit = req.param('limit');

    // if limit not sent, set standard value to 10
    if(!limit)
      limit = 10;

    // make sure all the parameters sent
    if(!videoId){
      missingParams.push('video_id');
    }

    if(missingParams.length > 0){
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
      isError = true;
      message = 'MissingParameters';
    }

    if(!isError){
      var query = Comment.find({video: videoId, is_deleted: false})
      .sort('createdAt DESC');
      if(since)
        query.where({timestamp: {'>': since}});
      else if(until)
        query.where({timestamp: {'<': until}});
      query
      .limit(limit)
      .populate('user')
      .exec(function (err, comments) {
        if(err){
          var errorString = JSON.stringify(err);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        return res.json(HTTP_STATUS.OK, { status: 'OK', data: comments });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }

  },

	store: function (req, res) {

    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var videoId = req.param('video_id');
    var content = req.param('content');

    // make sure all the parameters sent
    if(!videoId){
      missingParams.push('video_id');
    }
    if(!content){
      missingParams.push('content');
    }

    if(missingParams.length > 0){
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
      isError = true;
      message = 'MissingParameters';
    }

    if(!isError){
      var userId = req.user.id;
      Video.findOne({id: videoId})
      .exec(function (errorVideo, video) {
        if(errorVideo){
          var errorString = JSON.stringify(errorVideo);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[errorVideo.code] ? HTTP_STATUS[errorVideo.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        if(!video)
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'NotFound'});

        var params = {
          user: userId,
          video: videoId,
          content: content
        };
        Comment.create(params)
        .exec(function (errorComment, comment) {
          if(errorComment){
            var errorString = JSON.stringify(errorComment);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[errorComment.code] ? HTTP_STATUS[errorComment.code] : 400;
            sails.log.error(req.method + " "+ req.path + ": " + errorString);
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }

          return res.json(HTTP_STATUS.OK, {status: 'OK', data: comment});
        });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }
  },

  destroy: function (req, res) {
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var videoId = req.param('video_id');
    var commentId = req.param('comment_id');

    // make sure all the parameters sent
    if(!videoId){
      missingParams.push('video_id');
    }
    if(!commentId){
      missingParams.push('comment_id');
    }

    if(missingParams.length > 0){
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
      isError = true;
      message = 'MissingParameters';
    }

    if(!isError){
      Comment.findOne({id: commentId, video: videoId})
      .exec(function (err, comment) {
        if(err){
          var errorString = JSON.stringify(err);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        if(!comment)
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'NotFound'});

        comment.is_deleted = true;
        comment.save(function (e) {
          if(e){
            var errorString = JSON.stringify(e);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[e.code] ? HTTP_STATUS[e.code] : 400;
            sails.log.error(req.method + " "+ req.path + ": " + errorString);
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }

          return res.json(HTTP_STATUS.OK, {status: 'OK', message: 'DELETED' });
        });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }
  }
};

