/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * `AuthController.login()`
   */
   login: function (req, res) {

    // get parameter
    var email = req.param('email');
    var password = req.param('password');

    if (!email || !password) {
      return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'MissingParameters'});
    }

    User.findOne({email:email}, function (err, user) {
      if(!user){
        return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'EmailNotFound'})
      }
      User.comparePassword(password, user, function (err, valid) {
        if (err) {
          return res.json(HTTP_STATUS.FORBIDDEN, {status: 'FAILED', message: 'forbidden'});
        }

        if (!valid) {
          return res.json(HTTP_STATUS.UNAUTHORIZED, {status: 'FAILED', message: 'InvalidCredentials'});
        } else {
          return res.json({
            status: "OK",
            data: user
          });
        }
      });
    });
  },

  /**
   * `AuthController.register()`
   */
   register: function (req, res) {

    // get parameter
    var email = req.param('email'),
    first_name = req.param('first_name'),
    last_name = req.param('last_name'),
    password = req.param('password');

    var isError = false;
    var message = null;
    var missingParams = [];
    var code = null;

    // make sure all the parameters sent
    if(!email){
      missingParams.push('email');
    }
    if(!first_name){
      missingParams.push('first_name');
    }
    if(!last_name){
      missingParams.push('last_name');
    }
    if(!password){
      missingParams.push('password');
    }

    if(missingParams.length > 0){
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
      isError = true;
      message = 'MissingParameters';
    }

    if(!isError){
      User.findOne({email:email}, function (err, user) {
        if (err) {
          var errorString = JSON.stringify(err);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        if (user) {
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: "FAILED", message: 'EmailAlreadyUsed'})
        }

        var paramRequest = {
          email: email, 
          first_name: first_name, 
          last_name: last_name, 
          password: password
        };
        User.create(paramRequest).exec(function (errorUser, user) {
          if (errorUser) {
            var errorString = JSON.stringify(errorUser);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[errorUser.code] ? HTTP_STATUS[errorUser.code] : 400;
            sails.log.error(req.method + " "+ req.path + ": " + errorString);
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }
          // If user created successfuly we return user and token as response
          if (user) {
            return res.json(HTTP_STATUS.OK, {status: "OK", data: user});
          }
        });
      });
    } else {
      res.json(HTTP_STATUS.BAD_REQUEST, {status: "FAILED", message: message});
    }
  },

/*
  AuthController.loginProvider()
  */

  loginProvider: function(req, res){
    var providers = ['facebook', 'soundcloud'],
    uid = req.param('uid'),
    access_token = req.param('access_token'),
    provider = req.param('provider');

    if(!provider){
      return res.json(400,{status: "FAILED", message:"ProviderNotknown"});
    } else{
      if(providers.indexOf(provider) < 0){
        return res.json(400,{status: "FAILED", message:"ProviderNotSupported"});
      }
    }

    if(!uid || !access_token){
      return res.json(400,{status: "FAILED", message:"MissingParameters"});
    }

    Socialaccount.findOne({uid:uid}, function (err, social_account) {
      if(!social_account){
        return res.json(400, {status: "FAILED", message: 'SocialAccountNotFound'})
      }

      Socialaccount.compareAccessToken(access_token, social_account, function (err, valid) {
        if (err) {
          return res.json(400, {status: "FAILED", message: 'forbidden'});
        }

        if (!valid) {
          return res.json(400, {status: "FAILED", message: 'InvalidAccessToken'});
        } else {
          res.json({
            status: "OK",
            data: social_account
          });
        }
      });

    });

  },

/* 
  AuthController.registerProvider()
  */    
  registerProvider: function(req, res){
   var providers = ['facebook', 'soundcloud'],
   uid = req.param('uid'),
   username = req.param('username'),
   name = req.param('name'),
   provider = req.param('provider'),
   avatar = req.param('avatar'),
   access_token = req.param('access_token');

   if(!provider){
    return res.json(400,{status: "FAILED", message:"ProviderNotknown"});
  } else{
    if(providers.indexOf(provider) < 0){
      return res.json(400,{status: "FAILED", message:"ProviderNotSupported"});
    }
  }

  if(!uid || !username || !name || !provider || !avatar || !access_token){
   return res.json(400, {status: "FAILED", message: 'MissingParameters'});
 }

 Socialaccount.findOne({uid:uid},function (err, social_account){

   if(err){
     return res.json(err.status, {err:err});
   }
   if(social_account){
     return res.json(400, {status: "FAILED", message: 'SocialAccountAlreadyUsed'})
   }

   var paramSocialaccount = {
     uid: uid,
     username: username,
     name: name,
     provider: provider,
     access_token: access_token,
     avatar: avatar,
   };

   var suid = require('rand-token').suid;
   var pass = suid(8);
   var splitName = name.split(' '),
   first_name = splitName[0] || null,
   last_name = splitName[1] || null;
   var paramUser = {first_name: first_name, last_name: last_name, avatar: avatar, password: pass};
   User.create(paramUser).exec(function (err, user) {
     if(err){
       return res.json(err.status, {status: "FAILED", message: err});
     }
     if(user){
      paramSocialaccount.user = user.id;
      Socialaccount.create(paramSocialaccount).exec(function (err, social_account){
       if(err){
         return res.json(err.status, {status: "FAILED", message: err});
       }
       if(social_account){
        User.find(user.id).populate('socialaccounts').exec(function (err, value) {
          if(err)
            return res.json(400, {status: "FAILED", message: err});
          return res.json(200, {status:"OK", data: value});
        });
      }
    });
    }
  });
 });
},

forgotPassword: function(req, res){
  var email = req.param('email');

  if(!email){
    sails.log.info(req.method + " "+ req.path + ": MissingParameters");
    return res.json(401, {status: "FAILED", message: 'MissingParameters'});
  }

  User.findOne({email: email})
  .then(function (user) {
    if (!user) {
      sails.log.info(req.method + " "+ req.path + ": EmailNotFound");
      return res.json(400, {status: "FAILED", message: "EmailNotFound"}); 
    }

    sails.services.mailservice
    .send('forgot-password', {
      to: email,
      from: 'noreply <noreply.valdlabs@gmail.com>',
      subject: 'Request to change password',
      content: user
    });

    return res.json(200, {status: "OK", message: "EmailSent"});
  })
  .catch(function (err) {
    sails.log.error(req.method + " "+ req.path + ": " + err);
    return res.json(400, {status: "FAILED", message: err});
  });
},

resetPassword: function(req, res){
  var token = req.param('token');

  if(!token){
    req.addFlash('error', "Sorry, this page isn't available, The link you followed may be broken, or the page may have been removed.");
    return res.redirect('/')
  }

  User.findOne({token:token}, function (err, user) {
    if(!user){
      req.addFlash('error', "Sorry, this page isn't available, The link you followed may be broken, or the page may have been removed.");
      return res.redirect('/')
    } else{
      return res.view('auth/reset', {
        user: user
      })
    }
  });
},

verifyPassword: function(req, res){
  var token = req.param('token'),
  password = req.param('newPassword'),
  passwordConfirmation = req.param('newPasswordConfirmation');

  if(!token || !password || !passwordConfirmation){
    return res.json(401, {status: "FAILED", message: 'MissingParameters'});
  }

  User.findOne({token:token}, function (err, user) {
    if(!user){
      req.addFlash('error', 'User not found');
      return res.view('auth/reset');
    } else{
      User.update({email:user.email},{password:password}).exec(function (err, user) {
        if (err) {
          req.addFlash('error', err.status);
          return res.view('auth/reset');
        }
        if (user) {
         req.addFlash('success', 'Your password has been reset. Please try logging in again.');
         return res.redirect('/')
       }
     });
    }
  });
}

};