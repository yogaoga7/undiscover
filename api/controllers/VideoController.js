/**
 * VideosController
 *
 * @description :: Server-side logic for managing Videos
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 * @author    :: valdlabs.com
 */

 module.exports = {

  /* VideosController.create() */

  create: function (req, res){
    var song_title = req.param('song_title'),
    artist = req.param('artist'),
    thumbnail = req.param('thumbnail'),
    location = req.param('location'),
    genreIds = req.param('genre_ids'),
    caption = req.param('caption');

    var uploadFile = req.file('file'),
    content = uploadFile._files[0].stream,
    headers = content.headers;


    var validated = true,
    errorMessages = [],
    fileParams = {},
    settings = {
      allowedTypes: ['video/mp4','application/x-mpegURL','video/MP2T','video/3gpp','video/x-flv','video/x-msvideo','video/x-ms-wmv'],

    };
    
    var isError = false;
    var message = null;
    var missingParams = [];
    var code = null;

    //Check File Video
    if(_.indexOf(settings.allowedTypes, headers['content-type']) === -1){

      return res.json(400,{status: "FAILED",message: "Wrong filetype"});
      
    } 

    if(!song_title){
      missingParams.push('song_title');
    }
    if(!artist){
      missingParams.push('artist');
    }
    if(!thumbnail){
      missingParams.push('thumbnail');
    }
    if(!location){
      missingParams.push('location');
    }
    if(!genreIds){
      missingParams.push('genre_ids');
    }
    if(!uploadFile){
      missingParams.push('uploadFile');
    }

    if(missingParams.length > 0){
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
      isError = true;
      message = "MissingParameters";
    }

    if(!isError){

      var genreToArray = genreIds.split(',');

   //   uploadFile.upload({dirname: '../../assets/videos'},function (err, file){
     uploadFile.upload({
      adapter: require('skipper-s3'),
      key: 'AKIAJSUSTX36BYRURYFQ',
      secret: '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',
      bucket: 'undiscovered-media-asia'
    }, function (err, file){ 

      var user_id = req.user.id;
      var params = {
       song_title: song_title,
       artist: artist,
       thumbnail: thumbnail,
       location: location,
       genres: genreToArray,
       video: file[0].extra['Location'],
       user: user_id,
       caption: caption,
     };

     Video.create(params).exec(function (err, video){
       if(err){
        return res.json(err.status, {status: "FAILED", message: err});
      }
      if(!video){
        return res.json(400, {status: "FAILED", message:"FORBIDDEN"});
      }
      var regex = /(#[a-z0-9][a-z0-9\-_]*)/ig;
      var regHashtag = caption.match(regex);
      var paramHastags = {
        user: user_id,
        videos: video.id,
        hastag: regHashtag
      };
      Hastag.create(paramHastags).exec(function (err, hastag){
        if(err){
          return res.json(err.status, {status: "FAILED", message: err});   
        }
      });
      return res.json(HTTP_STATUS.OK, {status: 'OK', data: video});
    });
   });
}else{
  return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
}

},

update: function (req, res) {
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var videoId = req.param('video_id');
    var songTitle = req.param('song_title');
    var artist = req.param('artist');
    var location = req.param('location');

    // make sure all the parameters sent
    if(!videoId){
      missingParams.push('video_id');
    }
    if(!songTitle && !artist && !location && !genreIds){
      missingParams.push('song_title');
      missingParams.push('artist');
      missingParams.push('location');
      missingParams.push('genre_ids');
    }

    if(missingParams.length > 0){
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
      isError = true;
      message = 'MissingParameters';
    }

    if(!isError){
      Video.findOne({ id: videoId, user: req.user.id })
      .exec(function (err, video) {
        if(err){
          var errorString = JSON.stringify(err);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        if(!video)
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'NotFound'});

        if(songTitle)
          video.song_title = songTitle;
        if(artist)
          video.artist = artist;
        if(location)
          video.location = location;

        video.save(function (e, result) {
         if(e){
          var errorString = JSON.stringify(e);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[e.code] ? HTTP_STATUS[e.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }
        return res.json(HTTP_STATUS.OK, {status: 'OK', data: result});
      });
      });
} else {
  return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
}

},

destroy: function (req, res) {
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var videoId = req.param('video_id');
    var userId = req.user.id;

    // make sure all the parameters sent
    if(!videoId){
      missingParams.push('video_id');
    }
    if(missingParams.length > 0){
      sails.log.error(req.method + " "+ req.path + " | error : Missing Parameter {" + missingParams.join(',') + "}");
      isError = true;
      message = 'MissingParameters';
    }

    if(!isError){
      Video.findOne({id: videoId, user: userId})
      .exec(function (error, video) {
        if(error){
          var errorString = JSON.stringify(error);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[error.code] ? HTTP_STATUS[error.code] : 400;
          sails.log.error(req.method + " "+ req.path + ": " + errorString);
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }
        if(!video)
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'NotFound'});

        Comment.update({video: videoId}, {is_deleted: true}).exec(console.log);
        video.is_deleted = true;
        video.save(function (err) {
          if(err){
            var errorString = JSON.stringify(err);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
            sails.log.error(req.method + " "+ req.path + ": " + errorString);
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }

          return res.json(HTTP_STATUS.OK, {status: 'OK', message: 'DELETED'});
        });
      });
} else {
  return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
}
}

};

