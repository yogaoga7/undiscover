/**
 * SearchController
 *
 * @description :: Server-side logic for managing Searches
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	search: function(req, res){
 		var Q = require('q');
 		
 		var keyword = req.param('keyword'),
 		type = req.param('type'),
 		offset = req.param('offset');

 		Q.all([
 			User.find({
 				or : [
 				{first_name: {'like' : '%'+keyword+'%'}},
 				{last_name: {'like' : '%'+keyword+'%'}},
 				]
 			}).then(),

 			Genre.findOne({name: keyword}).then(),
 			])
 		.spread(function (users, genres){

 			var userIds = [];
 			var genreIds = [];
 			var query = Video.find();

 			if(users){
 				users.forEach(function (user){
 					userIds.push(user.id);
 				});
 			}
 			if(genres){
 				genres.forEach(function (genre){
 					genreIds.push(genre.id);
 				});
 			} 

 			if(userIds.length != 0){
 				query.where({user: userIds}).populate('user').limit(offset);
 			}
 			if(genreIds.length != 0){
 				query.where({genres: genreIds}).populate('genres').limit(offset);
 			} 

 			if(userIds.length == 0){
 				query.where({
 					or: [
 					{song_title: {'like' : '%'+keyword+'%'}},
 					{artist: {'like' : '%'+keyword+'%'}},
 					]
 				}).limit(offset);
 			}
 			
 			query.exec(function (err, video){
 				if(err){
 					return res.json(400, {status: "Fail", err: err});
 				}
 				if(video){
 					return res.json(HTTP_STATUS.OK, {status: 'OK', data: video});
 				}
 			});
 		})
 		.fail(function (err){
 			return res.json(400, {status: "FAIL", err: err});
 		});	
 	},

 	findHastag: function(req, res){
 		var q = req.param('q');
 		console.log(q);
 		Hastag.find({
 			hastag : { 'like': '%'+q+'%' }
 		}).exec(function (err, activity){
 			if(err){
 				return res.json(400, {status: "FAILED", err:err});
 			}
 			if(activity){
 				return res.json(200, {status: "OK", result: {activity: activity}});
 			}
 		});
 	}

 };

