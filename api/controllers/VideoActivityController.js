/**
 * VideoActivityController
 *
 * @description :: Server-side logic for managing Videoactivities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	like: function(req, res){
 		var type = req.param('type'),
 		video = req.param('video_id');

 		var isError = false;
 		var message = null;
 		var missingParams = [];

 		if(!type){
 			var errorString = JSON.stringify(e);
 			var errorObject = JSON.parse(errorString);
 			var code = HTTP_STATUS[e.code] ? HTTP_STATUS[e.code] : 400;
 			return res.json(code, {status: 'FAILED', message: errorObject.summary});
 		}

 		if(!isError){
 			var senderId = req.user.id;
 			var params = {
 				type : type,
 				video : video,
 				user : senderId,
 			};
 			VideoActivity.findOrCreate(params).exec(function (err, activity){
 				if(err){
 					return res.json(400, {status:"FAILED", err: err});
 				}
 				if(activity){
 					return res.json(HTTP_STATUS.OK, {status: "OK", data: activity});
 				}
 			});
 		}
 	},

 	unlike: function(req, res){
 		var type = req.param('type'),
 		video = req.param('video_id');

 		var isError = false;
 		var message = null;
 		var missingParams = [];

 		if(!isError){
 			var token = req.headers.authorization;
 			User.findOne({token: token}).exec(function (err, user){
 				if(err){
 					return res.json(err.status, {status: "FAILED", err: err});
 				}
 				var senderId = user.id;
 				var params = {
 					video: video,
 					user: senderId,
 				};
 				VideoActivity.findOne(params).exec(function (err, activity){
 					if(err){
 						var errorString = JSON.stringify(err);
 						var errorObject = JSON.parse(errorString);
 						return res.json(err.status, {status: "FAILED", message: errorObject.summary});
 					}
 					if(!activity)
 						return res.json(400, {status: "FAILED", message: "Video Not Found"});
 					
 					activity.is_deleted = true;
 					activity.save(function (err, unlike){
 						if(err){
 							var errorString = JSON.stringify(err);
 							var errorObject = JSON.parse(errorString);
 							return res.json(err.status, {status: "FAILED", message: errorObject.summary});
 						}
 						return res.json(HTTP_STATUS.OK, {status: "OK", data: "UNLIKE"});	
 					});
 				});
 			});
 		}
 	},

 	list: function(req, res){
 		var video = req.param('video_id');
 		
 		VideoActivity.findOne({video: video}).exec(function (err, activity){
 			if(err){
 				return res.json(400,{status: "FAILED"});
 			}
 			if(activity){
 				User.findOne({id: activity.user}).exec(function (err, user){
 					
 					if(err){
 						var errorString = JSON.stringify(err);
 						var errorObject = JSON.parse(errorString);
 						return res.json(err.status, {status: "FAILED", message: errorObject.summary});
 					}
 					if(user){
 						return res.json(HTTP_STATUS.OK, {status: "OK", data: user});	
 					}
 					
 				});
 			}
 		});
 	}

 };

