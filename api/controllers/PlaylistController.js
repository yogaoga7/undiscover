/**
 * PlaylistController
 *
 * @description :: Server-side logic for managing playlists
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

  index: function (req, res) {
    Playlist.find({user: req.user.id})
    .populateAll()
    .exec(function (err, playlists) {
      if(err){
        var errorString = JSON.stringify(err);
        var errorObject = JSON.parse(errorString);
        var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
        return res.json(code, {status: 'FAILED', message: errorObject.summary});
      }

      return res.json(HTTP_STATUS.OK, {status: 'OK', data: playlists});
    });
  },

  show: function (req, res) {
    
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var playlistId = req.param('playlist_id');

    if(!playlistId){
      missingParams.push('playlist_id');
    }

    if(missingParams.length > 0){
      isError = true;
      message = 'MissingParams';
    }

    if(!isError){
      Playlist.findOne({id: playlistId})
      .populateAll()
      .exec(function (err, playlist) {
        if(err){
          var errorString = JSON.stringify(err);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        if(!playlist)
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'NotFound'});

        return res.json(HTTP_STATUS.OK, {status: 'OK', data: playlist});
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }
  },

  store: function (req, res) {

    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get parameter
    var title = req.param('title');
    var description = req.param('description');

    if(!title){
      missingParams.push('title');
    }

    if(missingParams.length > 0){
      isError = true;
      message = 'MissingParams';
    }

    if(!isError){

      var params = {
        title: title,
        description: description,
        user: req.user.id
      };
      Playlist.create(params)
      .exec(function (err, playlist) {
        if(err){
          var errorString = JSON.stringify(err);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        Playlist.findOne({id: playlist.id})
        .populateAll()
        .exec(function (error, result) {
          if(error){
            var errorString = JSON.stringify(error);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[error.code] ? HTTP_STATUS[error.code] : 400;
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }
          return res.json(200, {status: "OK", data: result});
        });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }
  },

  update: function (req, res) {
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    var playlistId = req.param('playlist_id');
    var title = req.param('title');
    var description = req.param('description');

    if(!playlistId){
      missingParams.push('playlist_id');
    }
    if(!title && !description){
      missingParams.push('title');
      missingParams.push('description');
    }

    if(missingParams.length > 0){
      isError = true;
      message = 'MissingParams';
    }

    if(!isError){
      Playlist.findOne({id: playlistId})
      .populateAll()
      .exec(function (error, playlist) {
        if(error){
          var errorString = JSON.stringify(error);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[error.code] ? HTTP_STATUS[error.code] : 400;
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }

        if(!playlist)
          return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: 'NotFound' });

        if(title)
          playlist.title = title;
        if(description)
          playlist.description = description;
        
        playlist.save();
        return res.json(HTTP_STATUS.OK, { status: 'OK', data: playlist });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }

  },

  storeTrack: function (req, res) {

    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get param
    var playlistId = req.param('playlist_id');
    var videoIds = req.param('video_ids');

    // make sure all the parameters sent
    if(!playlistId){
      missingParams.push('playlist_id');
    }
    if(!videoIds){
      missingParams.push('video_ids');
    }

    if(missingParams.length > 0){
      isError = true;
      message = 'MissingParams';
    }

    if(!isError){
      // get playlist by id
      Playlist.findOne({id: playlistId})
      .populate('videos')
      .exec(function (err, playlist) {
        if(err){
          var errorString = JSON.stringify(err);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }
        if(!playlist)
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: "NotFound"});

        var ids = [];
        if(videoIds){
          ids = videoIds.split(',');
        }
        ids.forEach(function (value) {
          var isExist = _.find(playlist.videos, function (v) {
            return v.id == value;
          });
          if(!isExist)
            playlist.videos.add(value);
        });
        playlist.save(function (e, list) {
          if(e){
            var errorString = JSON.stringify(e);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[e.code] ? HTTP_STATUS[e.code] : 400;
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }
          Video.find({id: ids})
          .populate('activities', {is_deleted: false, sort: 'createdAt DESC'})
          .populate('comments', {is_deleted: false, sort: 'createdAt DESC'})
          .populate('genres')
          .populate('user')
          .exec(function (errVid, videos) {
            if(errVid){
              var errorString = JSON.stringify(errVid);
              var errorObject = JSON.parse(errorString);
              var code = HTTP_STATUS[errVid.code] ? HTTP_STATUS[errVid.code] : 400;
              return res.json(code, {status: 'FAILED', message: errorObject.summary});
            }
            return res.json(HTTP_STATUS.OK, {status: 'OK', data: videos});
          });
        });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }

  },

  destroy: function (req, res) {
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get param
    var playlistId = req.param('playlist_id');

    // make sure all the parameters sent
    if(!playlistId){
      missingParams.push('playlist_id');
    }

    if(missingParams.length > 0){
      isError = true;
      message = 'MissingParams';
    }

    if(!isError){
      Playlist.findOne(playlistId)
      .exec(function (error, record) {
        if(error){
          var errorString = JSON.stringify(error);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[error.code] ? HTTP_STATUS[error.code] : 400;
          return res.json(code, {status: 'FAILED', message: errorObject.summary});
        }
        if(!record)
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'NotFound'});
        
        Playlist.destroy({id: playlistId}).exec(function (err) {
          if(err){
            var errorString = JSON.stringify(err);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }
          return res.json(HTTP_STATUS.OK, {status: 'OK', message: 'DELETED'});
        });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }
  },

  destroyTrack: function (req, res) {
    
    // initialize variable
    var isError = false;
    var message = null;
    var missingParams = [];

    // get param
    var playlistId = req.param('playlist_id');
    var videoId = req.param('video_id');

    // make sure all the parameters sent
    if(!playlistId){
      missingParams.push('playlist_id');
    }
    if(!videoId){
      missingParams.push('video_id');
    }

    if(missingParams.length > 0){
      isError = true;
      message = 'MissingParams';
    }

    if(!isError){
      Playlist.findOne({id: playlistId})
      .populate('videos')
      .exec(function (error, playlist) {
        if(error){
          var errorString = JSON.stringify(error);
          var errorObject = JSON.parse(errorString);
          var code = HTTP_STATUS[error.code] ? HTTP_STATUS[error.code] : 400;
          return res.json(code, {status: "FAILED", message: errorObject.summary});
        }

        if(!playlist)
          return res.json(HTTP_STATUS.BAD_REQUEST, {status: 'FAILED', message: 'NotFound'});

        playlist.videos.remove(videoId);
        playlist.save(function (err, result) {
          if(err){
            var errorString = JSON.stringify(err);
            var errorObject = JSON.parse(errorString);
            var code = HTTP_STATUS[err.code] ? HTTP_STATUS[err.code] : 400;
            return res.json(code, {status: 'FAILED', message: errorObject.summary});
          }

          return res.json(HTTP_STATUS.OK, {status: 'OK', message: "DELETED"});
        });
      });
    } else {
      return res.json(HTTP_STATUS.BAD_REQUEST, { status: 'FAILED', message: message });
    }
  }

};

